$(function() {
	$("#sidebar").offset() == undefined? sidebar = $("#writer-sidebar") : sidebar = $("#sidebar")
	offset = sidebar.offset();
	var topPadding = 80;
	$(window).scroll(function() {
		s_height = sidebar.css('height')
		if ($(window).scrollTop() < ($('#footer').offset().top - parseInt(s_height.substr(0,s_height.length-2)) - 80)){
			if ($(window).scrollTop() > offset.top) {
				x = $(window).scrollTop() - offset.top + topPadding
				sidebar.stop().animate({
					marginTop:  x
				});
			} else {
				sidebar.stop().animate({
					marginTop: 0
				});
			}
		}
	});
});