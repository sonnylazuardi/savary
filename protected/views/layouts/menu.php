<!-- <ul class="menu">
	<a href="myworld.html"><li class="selected">MY WORLD</li></a>
	<a href="posts.html"><li>POST</li></a>
	<a href="addpost.html"><li class="sub">ADD POST</li></a>
	<a href="comment.html"><li>COMMENT</li></a>
	<a href="#"><li>MY WISHLIST</li></a>
</ul> -->

<?php 
$this->widget('zii.widgets.CMenu', array(
 'items'=>array(
     // Important: you need to specify url as 'controller/action',
     // not just as 'controller' even if default acion is used.
     array('label'=>'Home', 'url'=>array('site/index')),
     // 'Products' menu item will be selected no matter which tag parameter value is since it's not specified.
     array('label'=>'Products', 'url'=>array('product/index'), 'items'=>array(
         array('label'=>'New Arrivals', 'url'=>array('product/new', 'tag'=>'new')),
         array('label'=>'Most Popular', 'url'=>array('product/index', 'tag'=>'popular')),
     )),
     array('label'=>'Login', 'url'=>array('site/login'), 'visible'=>Yii::app()->user->isGuest),
 ),
));
?>