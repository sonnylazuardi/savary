<div id="header" class="full-width">
	<div class="row visible-overflow" style="position:relative">
		<div class="small-6 large-3 columns">
			<a href=""><img src="<?php echo Yii::app()->baseUrl ?>/img/logo.png" alt="Savary.Co" /></a>
		</div>
		<div class="large-6 columns center-align hide-for-small">
			<h5 class="large-3 columns"><a href="#story">STORY</a></h5>
			<form class="large-6 columns search-wrap">
				<input type="text" class="large-9 columns" />
				<button type="submit" class="large-3 columns" >go!</button>
			</form>
			<h5 class="large-3 columns"><a href="#gallery">GALLERY</a></h5>
		</div>
		<?php $this->renderPartial('//layouts/login-form') ?>
	</div>
</div>