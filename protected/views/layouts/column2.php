<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
<div id="sidebar" class="large-3 columns hide-for-small">
	<?php $this->renderPartial('//layouts/profile') ?>
	<?php $this->renderPartial('//layouts/menu') ?>
</div>

<div id="main-content" class="large-9 columns">
	<div class="row">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
</div>
<?php $this->endContent(); ?>