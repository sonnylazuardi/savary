<?php if (Yii::app()->user->isGuest): ?>
<div class="small-6 large-3 columns" style="position:relative">
	<h5 class="right-align"><a href="#" class="login-button">login</a></h5>
	<form class="login-form" method="post" action="<?php echo Yii::app()->createUrl('/site/login') ?>">
		<div class="row">
			<div class="small-4 columns">
			  <label for="right-label" class="right inline">USERNAME</label>
			</div>
			<div class="small-8 columns">
			  <input type="text" name="LoginForm[username]" />
			</div>
		</div>
		<div class="row">
			<div class="small-4 columns">
			  <label for="right-label" class="right inline">PASSWORD</label>
			</div>
			<div class="small-8 columns">
			  <input type="password" name="LoginForm[password]"/>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns right-align">
				<button type="submit" class="btn-orange">LOGIN</button>
			</div>
		</div>
	</form>
</div>
<?php else: ?>
<div class="small-6 large-3 columns right">
<h5 class="large-8 columns right-menu">
	<a href="<?php echo Yii::app()->createUrl('/user/profile') ?>"><?php echo Yii::app()->user->name ?></a>
</h5>
<div class="large-2 columns right-menu">
	<a href="<?php echo Yii::app()->createUrl('/site/setting') ?>">
		<img class="hide-for-small" src="img/setting.png" alt="setting">
		<span class="show-for-small">settings</span>
	</a>
</div>
<div class="large-2 columns right-menu">
	<a href="<?php echo Yii::app()->createUrl('/site/logout') ?>">
		<img class="hide-for-small" src="img/logout.png" alt="logout">
		<span class="show-for-small">logout</span>
	</a>
</div>
</div>
<?php endif; ?>