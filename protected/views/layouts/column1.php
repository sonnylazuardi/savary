<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="main-content" class="large-12 columns">
	<?php echo $content; ?>
</div><!-- content -->
<?php $this->endContent(); ?>