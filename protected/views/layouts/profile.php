<?php if (!Yii::app()->user->isGuest): ?>
	<div class="row profile">
		<div class="large-7 columns left">
			<h5 class="center-align">Traveler</h5>
			<img src="img/android.png">
			<div class="center-align h8">Pejalan Kaki</div>
			<div id="back-bar">
				<div id="post-bar"></div>
			</div>
			<div class="center-align h9"><span id="counter">3</span><span>/5 POST</span></div>
		</div>
		<div class="large-5 columns right">
			<div class="h8">LAST BADGE</div>
			<img src="img/android.png" alt="achievement">
			<div class="h8">STATS</div>
			<ul>
				<li class="h9"><span id="counter">2000</span><span> f</span></li>
				<li class="h9"><span id="counter">10</span><span> b</span></li>
			</ul>
		</div>
	</div>
<?php endif ?>