<div id="footer" class="full-width">
	<div class="row">
		<div class="large-5 columns">
			<h3>ABOUT<b>US</b></h3>
			<p>
				Savary adalah portal berbagi informasi perjalanan wisata yang ada di Indonesia. Layanan ini diharapkan dapat membantu persiapan para traveler untuk mengarungi dunia pariwisata Indonesia serta memperluas pengetahuan tentang tempat wisata di Indonesia. Mari kita nikmati keindahan alam ibu pertiwi, bersama-sama. <i>Travel, Share.</i>
			</p>
			<h3>SAVARY<b>.CO</b></h3>
			<p>
				<a href="#terms">Aturan Penggunaan</a> &sdot; <a href="#privacy">Kebijakan Privasi</a><br />
				v 0.1, 9 Mei 2013
			</p>
		</div>
		<div class="large-3 columns">
			<h3>FIND<b>US</b></h3>
			<ul>
				<li><div class="minicircle"></div>Facebook</li>
				<li><div class="minicircle"></div>Twitter</li>
				<li><div class="minicircle"></div>Tumblr</li>
				<li><div class="minicircle"></div>Google +</li>
				<li><div class="minicircle"></div>Youtube</li>
				<li><div class="minicircle"></div>Instagram</li>
				<li><div class="minicircle"></div>Subscribe</li>
			</ul>
		</div>
		<div class="large-4 columns">
			<h3>CONTACT<b>US</b></h3>
			<p>
				Labtek V<br />
				Institut Teknologi Bandung<br />
				Jalan Ganeca 10 - Jawa Barat<br />
				Indonesia<br />
				+6281809016758<br />
				savary@motherlode.co
			</p>
		</div>
	</div>
</div>