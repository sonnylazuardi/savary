<!DOCTYPE HTML>
<html>
	<head>
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/foundation.css" />
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/normalize.css" />
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts/Museo_Slab.css" />
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts/Museo_Sans.css" />
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/savary.css" />
		<meta name="viewport" content="user-scalable=yes, initial-scale=1.0, maximum-scale=2.0, width=device-width" />
	</head>
	<body>
		<?php $this->renderPartial('//layouts/header') ?>		
		<div id="content">
			<?php echo $content ?>
		</div>
		<?php $this->renderPartial('//layouts/footer') ?>
		<script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/jquery.js'></script>
		<script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/js/savary2.js'></script>
	</body>
</html>