<li class="large-6 columns">
	<div class="wrapper">
		<div class="date"><?php echo date('j F Y', strtotime($data->timestamp)) ?></div>
		<div class="row">
			<a href="<?php echo Yii::app()->createUrl('/post/view', array('id'=>$data->id)) ?>"><h3 class="small-10 columns"><?php echo $data->title ?></h3></a>
		</div>
		<div class="row">
			<div class="small-5 columns author">
				by <a href="#"><?php echo $data->user->name ?></a>
			</div>								
			<ul class="tag-label right-align small-7 columns">
				<li class="nature">nature</li>
				<li class="mount">lake</li>
				<li class="city">medan</li>
			</ul>
		</div>
		<div class="row center-align">
			<div class="picture">
				<img src="img/fotoh_4.jpg" alt="gambar" />
			</div>
		</div>
		<div class="row">
			<div class="small-10 small-offset-1 columns">
				<p><?php echo $data->description ?></p>
			</div>
		</div>
	</div>
	<ul class="elements-info">
		<li class="story"></li>
		<li class="homestay"></li>
		<li class="additional"></li>
	</ul>
</li>

<!-- 	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('place')); ?>:</b>
	<?php echo CHtml::encode($data->place); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('picture')); ?>:</b>
	<?php echo CHtml::encode($data->picture); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timestamp')); ?>:</b>
	<?php echo CHtml::encode($data->timestamp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br /> -->