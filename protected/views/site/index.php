<div class="full-width home-sliding-image">
	<div class="slight-overlay"></div>
	<div class="row">
		<div class="large-7 columns hide-for-small featured-story">
			<a href="#"><h2>Sok-sokan Berkemah di Pine Forest Camp</h2></a>
			<div class="row">
			<ul class="element-label large-8 columns">
				<li class="story">story</li>
				<li class="timeline">timeline</li>
				<li class="budgeting">budgeting</li>
				<li class="homestay">homestay</li>
				<li class="additional">additional</li>
			</ul>
			</div>
			<h4>by <a href="#">PenumpangAngkot</a></h4>
		</div>
		<div class="large-4 columns large-offset-1">
			<div class="large-11 large-offset-1 columns home-search-wrap visible-overflow">
				<h3>CARI TUJUANMU</h3>
				<form class="row">
					<div class="large-12 columns">
						<input type="text" class="small-10 columns" />
						<button type="submit" class="small-2 columns" >go</button>
					</div>
				</form>
				<p>tempat wisata, dimana saja</p>
				<div class="row visible-overflow">
					<h2 class="top-post-header">TOP POST</h2>
				</div>
				<ul class="featured-stories row">
					<li class="small-12 columns">
						<a href="#"><h4>1. Perjalanan Menaklukkan Puncak Semeru</h4></a>
						<ul class="element-label">
							<li class="story">story</li>
							<li class="timeline">timeline</li>
							<li class="budgeting">budgeting</li>
							<li class="homestay">homestay</li>
							<li class="additional">additional</li>
						</ul>
					</li>
					<li class="small-12 columns">
						<a href="#"><h4>2. Trip Impulsif Tanpa Pikir Panjang</h4></a>
						<ul class="element-label">
							<li class="story">story</li>
							<li class="timeline">timeline</li>
							<li class="budgeting">budgeting</li>
						</ul>
					</li>
				</ul>
				<div class="row">
					<div class="large-6 large-centered columns center-align nav-buttons">
						<img src="img/home-prev.png" alt="previous stories" />
						<img src="img/home-next.png" alt="next stories" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="full-width title-bg">
	<div class="row">
		<h3 class="latest-story-title">Latest Stories</h3>
	</div>
</div>
<div class="row">
	<ul class="stories">
		<li class="large-4 columns">
			<div class="wrapper">
				<div class="date">8 Mei '13</div>
				<div class="row">
					<a href="#"><h3 class="small-10 columns">Perjalanan Menaklukkan Puncak Semeru</h3></a>
				</div>
				<div class="row">
					<div class="small-5 columns author">
						by <a href="#">PejalanKaki</a>
					</div>								
					<ul class="tag-label right-align small-7 columns">
						<li class="nature">nature</li>
						<li class="mount">mountain</li>
						<li class="city">malang</li>
					</ul>
				</div>
				<div class="row center-align">
					<div class="picture">
						<img src="img/fotoh_1.jpg" alt="gambar" />
					</div>
				</div>
				<div class="row">
					<div class="small-10 small-offset-1 columns">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
			</div>
			<ul class="elements-info">
				<li class="story"></li>
				<li class="budgeting"></li>
				<li class="additional"></li>
			</ul>
		</li>
		<li class="large-4 columns">
			<div class="wrapper">
				<div class="date">10 Apr '13</div>
				<div class="row">
					<a href="#"><h3 class="small-10 columns">Trip Impulsif Tanpa Pikir Panjang</h3></a>
				</div>
				<div class="row">
					<div class="small-5 columns author">
						by <a href="#">PejalanKaki</a>
					</div>								
					<ul class="tag-label right-align small-7 columns">
						<li class="nature">nature</li>
						<li class="mount">lake</li>
						<li class="city">medan</li>
					</ul>
				</div>
				<div class="row center-align">
					<div class="picture">
						<img src="img/fotoh_4.jpg" alt="gambar" />
					</div>
				</div>
				<div class="row">
					<div class="small-10 small-offset-1 columns">
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</div>
			</div>
			<ul class="elements-info">
				<li class="story"></li>
				<li class="homestay"></li>
				<li class="additional"></li>
			</ul>
		</li>
		<li class="large-4 columns">
			<div class="wrapper">
				<div class="date">8 Mei '13</div>
				<div class="row">
					<a href="#"><h3 class="small-10 columns">Perjalanan Menaklukkan Puncak Semeru</h3></a>
				</div>
				<div class="row">
					<div class="small-5 columns author">
						by <a href="#">PejalanKaki</a>
					</div>								
					<ul class="tag-label right-align small-7 columns">
						<li class="nature">nature</li>
						<li class="mount">mountain</li>
						<li class="city">malang</li>
					</ul>
				</div>
				<div class="row center-align">
					<div class="picture">
						<img src="img/fotoh_1.jpg" alt="gambar" />
					</div>
				</div>
				<div class="row">
					<div class="small-10 small-offset-1 columns">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
			</div>
			<ul class="elements-info">
				<li class="story"></li>
				<li class="timeline"></li>
				<li class="budgeting"></li>
			</ul>
		</li>
		<li class="large-4 columns">
			<div class="wrapper">
				<div class="date">10 Apr '13</div>
				<div class="row">
					<a href="#"><h3 class="small-10 columns">Trip Impulsif Tanpa Pikir Panjang</h3></a>
				</div>
				<div class="row">
					<div class="small-5 columns author">
						by <a href="#">PejalanKaki</a>
					</div>								
					<ul class="tag-label right-align small-7 columns">
						<li class="nature">nature</li>
						<li class="mount">lake</li>
						<li class="city">medan</li>
					</ul>
				</div>
				<div class="row center-align">
					<div class="picture">
						<img src="img/fotoh_4.jpg" alt="gambar" />
					</div>
				</div>
				<div class="row">
					<div class="small-10 small-offset-1 columns">
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</div>
			</div>
			<ul class="elements-info">
				<li class="story"></li>
				<li class="timeline"></li>
				<li class="budgeting"></li>
				<li class="homestay"></li>
				<li class="additional"></li>
			</ul>
		</li>
		<li class="large-4 columns">
			<div class="wrapper">
				<div class="date">1 Apr '13</div>
				<div class="row">
					<a href="#"><h3 class="small-10 columns">Hura-hura Liburan</h3></a>
				</div>
				<div class="row">
					<div class="small-5 columns author">
						by <a href="#">PejalanKaki</a>
					</div>								
					<ul class="tag-label right-align small-7 columns">
						<li class="nature">city</li>
						<li class="mount">skyscraper</li>
						<li class="city">jakarta</li>
					</ul>
				</div>
				<div class="row center-align">
					<div class="picture">
						<img src="img/fotoh_5.jpg" alt="gambar" />
					</div>
				</div>
				<div class="row">
					<div class="small-10 small-offset-1 columns">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
			</div>
			<ul class="elements-info">
				<li class="story"></li>
				<li class="timeline"></li>
				<li class="homestay"></li>
				<li class="additional"></li>
			</ul>
		</li>
		<li class="large-4 columns">
			<div class="wrapper">
				<div class="date">8 Mei '13</div>
				<div class="row">
					<a href="#"><h3 class="small-10 columns">Perjalanan Menaklukkan Puncak Semeru</h3></a>
				</div>
				<div class="row">
					<div class="small-5 columns author">
						by <a href="#">PejalanKaki</a>
					</div>								
					<ul class="tag-label right-align small-7 columns">
						<li class="nature">nature</li>
						<li class="mount">mountain</li>
						<li class="city">malang</li>
					</ul>
				</div>
				<div class="row center-align">
					<div class="picture">
						<img src="img/fotoh_1.jpg" alt="gambar" />
					</div>
				</div>
				<div class="row">
					<div class="small-10 small-offset-1 columns">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
			</div>
			<ul class="elements-info">
				<li class="story"></li>
				<li class="budgeting"></li>
				<li class="timeline"></li>
			</ul>
		</li>
		<li class="large-4 columns">
			<div class="wrapper">
				<div class="date">10 Apr '13</div>
				<div class="row">
					<a href="#"><h3 class="small-10 columns">Trip Impulsif Tanpa Pikir Panjang</h3></a>
				</div>
				<div class="row">
					<div class="small-5 columns author">
						by <a href="#">PejalanKaki</a>
					</div>								
					<ul class="tag-label right-align small-7 columns">
						<li class="nature">nature</li>
						<li class="mount">lake</li>
						<li class="city">medan</li>
					</ul>
				</div>
				<div class="row center-align">
					<div class="picture">
						<img src="img/fotoh_4.jpg" alt="gambar" />
					</div>
				</div>
				<div class="row">
					<div class="small-10 small-offset-1 columns">
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</div>
			</div>
			<ul class="elements-info">
				<li class="story"></li>
				<li class="budgeting"></li>
				<li class="additional"></li>
			</ul>
		</li>
		<li class="large-4 columns">
			<div class="wrapper">
				<div class="date">1 Apr '13</div>
				<div class="row">
					<a href="#"><h3 class="small-10 columns">Hura-hura Liburan</h3></a>
				</div>
				<div class="row">
					<div class="small-5 columns author">
						by <a href="#">PejalanKaki</a>
					</div>								
					<ul class="tag-label right-align small-7 columns">
						<li class="nature">city</li>
						<li class="mount">skyscraper</li>
						<li class="city">jakarta</li>
					</ul>
				</div>
				<div class="row center-align">
					<div class="picture">
						<img src="img/fotoh_5.jpg" alt="gambar" />
					</div>
				</div>
				<div class="row">
					<div class="small-10 small-offset-1 columns">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
			</div>
			<ul class="elements-info">
				<li class="story"></li>
				<li class="timeline"></li>
				<li class="budgeting"></li>
				<li class="homestay"></li>
				<li class="additional"></li>
			</ul>
		</li>
		<div class="clear"></div>
	</ul>
</div>

<div id="partners" class="full-width">
	<div class="row">
		<h3 class="large-12 columns center-align">OUR<b>PARTNERS</b></h3>
	</div>
	<ul class="row">
		<li class="large-3 columns">
			<a href="#"><div class="partner-logo"></div></a>
		</li>
		<li class="large-3 columns">
			<a href="#"><div class="partner-logo"></div></a>
		</li>
		<li class="large-3 columns">
			<a href="#"><div class="partner-logo"></div></a>
		</li>
		<li class="large-3 columns">
			<a href="#"><div class="partner-logo"></div></a>
		</li>
		<div class="clear"></div>
	</ul>
</div>